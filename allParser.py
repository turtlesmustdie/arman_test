import sys
from contextlib import ExitStack
def parsMy(mainHeader,addressList):
    mainIncludes = []
    addressIncludes = []
    mainDefines = []
    mainExtern = []
    with open (mainHeader,'r') as mainFile:
        for lines in mainFile:
            if("#include" in lines):
               lines = lines.split()
               lines= lines[1]
               lines = lines.replace('"','')
               mainIncludes.append(lines)  
            if("#define" in lines):
               lines = lines.split()
               mainDefines.append(lines)
            if ("extern" in lines):
                lines = lines.split()
                mainExtern.append(lines[0:3])
    for k in addressList:
        k = k.split('/')
        k = k[-1]
        addressIncludes.append(k)
    neededFiles = set(mainIncludes).difference(addressIncludes)
    if (not neededFiles):
        print("All dependency met")
    else:
        print("follwing include files are needed:")
        print(neededFiles)
        sys.exit()
    allDependIncludes = []  
    for files in addressList:
        with open (files,'r') as f:
            for lines in f:
                if("#include" in lines):
                   lines = lines.split()
                   lines = lines[1]
                   lines = lines.replace('"','')
                   if((lines not in addressIncludes) and (lines not in mainIncludes)):
                       allDependIncludes.append(lines)
    newNeededFiles = set(allDependIncludes).difference(addressIncludes)
    if(not newNeededFiles):
        print("All Passed")
    else:
        print("follwing include files are needed:")
        print(newNeededFiles)
        sys.exit()
    allDefines = []
    allExtern = []
    for allFiles in addressList:
        with open(allFiles,'r') as d:
            for lines in d:
                lines = lines.split()
                if ("#define" in lines):
                    allDefines.append(lines)
                if("extern" in lines):
                    allExtern.append(lines)
                    
    
    mixExtern = mainExtern + allExtern        ###### Holds all the Externs                
    mixDefines = mainDefines + allDefines     ###### Holds all the Defines in it
    mainStructStartLine = []
    enumStartLine = []
    mainFileToList = []
    with open (mainHeader,'r') as reading:
        for indexes,lines in enumerate (reading):
            mainFileToList.append(lines)
            if ("typedef enum" in lines):
                enumStartLine.append(indexes)
            if("typedef struct" in lines):
                mainStructStartLine.append(indexes)
    mainStructEndLine = []
    with open(mainHeader,'r') as reads:
        for numsX,indicesX in enumerate(reads):
            for i in mainStructStartLine:
                if(numsX>i):
                    if ("}" in indicesX):
                        mainStructEndLine.append(numsX+1)
                        break

    structCount = 0
    mainStructs = []  ####### Holds Structs in main header file 
    for i in mainStructStartLine:
        mainStructs.append(mainFileToList[mainStructStartLine[structCount]:mainStructEndLine[structCount]])
        structCount += 1
        
    
    enumEndLine = []
    with open(mainHeader,'r') as reads:
        for nums,indices in enumerate(reads):
            for i in enumStartLine:
                if(nums>i):
                    if ("}" in indices and "Param" not in indices and "Input" not in indices and "Output" not in indices):
                        enumEndLine.append(nums+1)
                        break
    mainEnums = []
    enumCount = 0              ####### Holds enums in mainHeader File   
    for i in enumStartLine:
        mainEnums.append(mainFileToList[enumStartLine[enumCount]:enumEndLine[enumCount]])
        enumCount += 1
    
    tempValues = []
    finalHeaderEnums = [] ################## has a clean list of Enums from mainHeader File
    for j,k in enumerate(mainEnums): 
        for lines in k:
            lines = lines.split()
            values = lines[2:3]
            if(values):
                tempValues.append(values)
        tempValues.append(lines[-1])
        finalHeaderEnums.append(tempValues)
        tempValues = []
    myEnumFiles = []
    TmpEnumFiles = []
    finalEnumFiles =  []
    myStructFiles = []
    for files in addressList:
        with open (files,'r') as myFiles:
            for content in myFiles:
                if ("typedef enum" in content):
                    myEnumFiles.append(files)
                    break
                if ("typedef struct" in content):
                    myStructFiles.append(files)
                    break
    for files in addressList:
        with open (files,'r') as myFiles:
            for content in myFiles:
                if ("typedef struct" in content):
                    myStructFiles.append(files)
                    break
    
    for Kfiles in myEnumFiles:
        with open (Kfiles,'r') as KmyFiles:
            for contents in KmyFiles:
                TmpEnumFiles.append(contents)
                
        finalEnumFiles.append(TmpEnumFiles)
        TmpEnumFiles =[]
            
    otherStructStart = []
    otherFinalStructStart = []    
    for g in myStructFiles:
        with open(g,'r') as files:
            for numbers,myVals in enumerate(files):
                if ("typedef struct" in myVals):
                    otherStructStart.append(numbers+1)
            if(otherStructStart):
                otherFinalStructStart.append(otherStructStart)
            otherStructStart = []

    tmpStructEnd = []
    otherFinalStructEnd = []        
    sCount = 0
    for ee in otherFinalStructStart[sCount]:
        with open(myStructFiles[sCount],'r') as mFiles:
            for ii,kk in enumerate(mFiles):
                if(ii>ee):
                    if ("}" in kk and "Param" not in kk and "Input" not in kk and "Output" not in kk):
                        tmpStructEnd.append(ii+1)
                        
                
        otherFinalStructEnd.append(tmpStructEnd)
        tmpEnumEnd = []
        sCount+=1
    
    #print(otherFinalStructStart)
    #print(otherFinalStructEnd)
    
#    myFinalStructTmp = []
#    theFinalStruct = []
#    eCount = 0
#    myInnerCount = 0
#    for s in myStructFiles:
#        for l in otherFinalStructStart[eCount]:
#            myFinalStructTmp.append(s[otherFinalStructStart[eCount][myInnerCount]:otherFinalStructEnd[eCount][myInnerCount]])
#            theFinalStruct.append(myFinalStructTmp)
#            myFinalEnumsTemps = []
#            myInnerCount += 1
#        myInnerCount = 0
#        eCount += 1
    
    
    otherEnumStart = []
    finalEnumStart = []            
    for g in myEnumFiles:
        with open(g,'r') as files:
            for numbers,myVals in enumerate(files):
                if ("typedef enum" in myVals):
                    otherEnumStart.append(numbers+1)
            finalEnumStart.append(otherEnumStart)
            otherEnumStart = []
            
    tmpEnumEnd = []
    finalEnumEnd = []
    mCount = 0
    for i in finalEnumStart[mCount]:
        with open(myEnumFiles[mCount],'r') as mFiles:
            for ii,kk in enumerate(mFiles):
                if(ii>i):
                    if ("}" in kk and "Param" not in kk and "Input" not in kk and "Output" not in kk):
                        tmpEnumEnd.append(ii+1)
                        
        finalEnumEnd.append(tmpEnumEnd)
        tmpEnumEnd = []
        mCount+=1                
    myFinalEnumsTemps = []
    theFInalNums = []
    nCount = 0
    innerCount = 0
    for s in finalEnumFiles:
        for l in finalEnumStart[nCount]:
            myFinalEnumsTemps.append(s[finalEnumStart[nCount][innerCount]:finalEnumEnd[nCount][innerCount]])
            theFInalNums.append(myFinalEnumsTemps)
            myFinalEnumsTemps = []
            innerCount += 1
        innerCount = 0
        nCount += 1
    
    nTemp = []
    nFinalNums = []
    for ddd in theFInalNums:
        for lines in ddd:
            for lineX in lines:
                lineX = lineX.split()
                mVals = lineX[2:3]
                if(mVals):
                    nTemp.append(mVals)
            nTemp.append(lines[-1])
            nFinalNums.append(nTemp)
            nTemp = []
    allEnums = []
    allEnums = nFinalNums + finalHeaderEnums ##### holds All the Enums from all the files
    
    
    
    
                
    
     

MymainHeader = 'C:/Data/Parser/OSRCTA_CritLevSel_TtcCalc.h'
otherList = ['C:/Data/Parser/ABST_Types.h','C:/Data/Parser/ABST_PrjTypes.h','C:/Data/Parser/ABST_BaseTypes.h','C:/Data/Parser/ABST_Config.h']
parsMy(MymainHeader,otherList)