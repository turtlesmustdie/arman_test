/**
 * Automotive Safety Technologies GmbH
 *
 * @brief      This header abstract the base types for the PPE project
 * @project    Nanoradar_PPE
 * @file       ABST_BaseTypes.h
 * @version    MKS: $Revision: 1.1 $
 * @compliance ISO/IEC 9899:1990 (C90), MISRA-C:2012, ISO-26262-6, AutoSar 3.2, VW LAH 893.909
 * @date       MKS: $Date: 2020/05/18 12:22:16CEST $
 * @copyright  Copyright (C) 2020, Automotive Safety Technologies GmbH. All rights reserved.
 * @details    This header abstract the base types for the PPE project. 
 * @link       IRS: 
 * @link       SDRS: 
 * @history    -
 **/

#ifndef ABST_BASETYPES_H
#define ABST_BASETYPES_H

/*############################################################################
 # INCLUDES
 ############################################################################*/

/*############################################################################
 # DEFINES
 ############################################################################*/
/**
 * @name Version handlers.
 * @details The file version should be added here for interface compability check.
 * @{
 */
#define MAJOR_VERSION_PPE_BASE_TYPES_H (1)
#define MINOR_VERSION_PPE_BASE_TYPES_H (1)
#define PATCH_VERSION_PPE_BASE_TYPES_H (0)
 /** @} */

/*############################################################################
 # TYPE DEFINITIONS
 ############################################################################*/
/**
 * @name NR PPE Base Types
 * @details All for base types for this project are specified here and shall be encapsulated later in a header file. Proposal PPE_Base_Types.h.
 * @{ */
typedef unsigned char tBool;
typedef short tInt16;
typedef long tInt32;
typedef unsigned char tUInt8;
typedef unsigned short tUInt16;
typedef unsigned long tUInt32;
typedef float tFloat32;
 /** @} */

/*############################################################################
 # GLOBAL VARIABLES
 ############################################################################*/

/*############################################################################
 # FUNCTION PROTOTYPES
 ############################################################################*/

#endif /* ABST_BASETYPES_H */

/* EOF */
