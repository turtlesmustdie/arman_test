/**
 * Automotive Safety Technologies GmbH
 *
 * \brief      ASTech abstracted type definitions.
 * \project    ASTech Coding Guidelines
 * \file       ABST_Types.h
 * \version    MKS: $Revision: 1.1 $
 * \compliance ISO/IEC 9899:1990 (C90), MISRA-C:2016, ISO-26262-6, AutoSAR 3.2, VW LAH 893.909
 * \date       MKS: $Date: 2020/05/18 12:22:16CEST $
 * \copyright  Copyright (C) 2018, Automotive Safety Technologies GmbH. All rights reserved.
 * \details    Those types must be abstracted in order to become platform and compiler independent.
 * \history    -
 **/

#ifndef ABST_TYPES_H
#define ABST_TYPES_H

/*############################################################################
 # INCLUDES
 ############################################################################*/
 #include "ABST_BaseTypes.h" /* Abstract the base types for this project */
 #include "ABST_Config.h" /* Includes the compiler switches and configured constants */

/*############################################################################
 # DEFINES
 ############################################################################*/
/**
 * @name Version handlers.
 * @details The file version should be added here for interface compability check.
 * @{
 */
#define MAJOR_VERSION_ABST_TYPES_H (1)
#define MINOR_VERSION_ABST_TYPES_H (0)
#define PATCH_VERSION_ABST_TYPES_H (0)
 /** @} */

/**
 * @brief   Defines the physical constant PI.
 * @details Shall be used for floating point operations.
 */
#define ABST_PI (3.1415926535897932384626433f)

/**
 * @brief   Defines boolean FALSE
 * @details Shall be used as values for the boolean datatype.
 */
#ifndef FALSE
#define ABST_FALSE (0u)
#else
#define ABST_FALSE FALSE
#endif

/**
 * @brief   Defines boolean TRUE
 * @details Shall be used as values for the boolean datatype.
 */
#ifndef TRUE
#define ABST_TRUE (1u)
#else
#define ABST_TRUE TRUE
#endif

/**
 * @brief   Definition of null.
 * @details Shall define the value null.
 */
#ifndef NULL
#define ABST_NULL (0)
#else
#define ABST_NULL NULL
#endif

/**
 * @brief   Definition of null pointer.
 * @details Shall define the adress which represent a null pointer.
 */
#ifndef NULL_PTR
#define ABST_NULL_PTR ( (void *) 0)
#else
#define ABST_NULL_PTR NULL_PTR
#endif

/*############################################################################
 # TYPE DEFINITIONS
 ############################################################################*/

#if 0
/**
 * @brief   Defines the error states for abstracted functions or macros.
 * @details The error shall be reported if the function or macro execution can cause a problem or is not defined in a specific range.
 */
typedef enum {
	Abst_Math_Nok = ABST_FALSE, /** A unspecific error are detected. */
	Abst_Math_Ok = ABST_TRUE /** No error are detected. */
} eAbstMathError;


typedef enum {
	Abst_Math_Nok = ABST_FALSE, /** A unspecific error are detected. */
	Abst_Math_Ok = ABST_TRUE /** No error are detected. */
} koskalak;

typedef struct {
	Abst_Math_Nok = ABST_FALSE, /** A unspecific error are detected. */
	Abst_Math_Ok = ABST_TRUE /** No error are detected. */
} kooli;

typedef struct {
	Abst_Math_Nok = ABST_FALSE, /** A unspecific error are detected. */
	Abst_Math_Ok = ABST_TRUE /** No error are detected. */
} kooli;
#endif

/*############################################################################
 # GLOBAL VARIABLES
 ############################################################################*/
 
/*############################################################################
 # FUNCTION PROTOTYPES
 ############################################################################*/
 
#endif /* ABST_TYPES_H */

/* EOF */
